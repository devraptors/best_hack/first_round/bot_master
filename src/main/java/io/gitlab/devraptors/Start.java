package io.gitlab.devraptors;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import modules.mongodb.MongoInteraction;
import modules.rabbit.Rabbit;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Start {

    public static class Ticket {
        public String from;
        public String to;
        public String ticket;

        public Ticket(String from, String to, String ticket) {
            this.from = from;
            this.to = to;
            this.ticket = ticket;
        }
    }


    public static ArrayList<Ticket> requestToFindTickets(JSONObject object) throws Exception {
        JSONArray jsonArray = (JSONArray) object.get("to");
        ArrayList<String> array = new ArrayList<>();
        for (int i=0;i<jsonArray.length();i++) {
            array.add(jsonArray.getString(i));
        }
        List<Ticket> result = new ArrayList<>();

        for (String s : array) {
            Ticket ticket = new Ticket(object.getString("from"), s, checkTickets(object.getString("from"), s));
            result.add(ticket);
        }
        return (ArrayList<Ticket>) result;
    }

    public static void main(String[] args) throws Exception {
        MongoInteraction.MongoConfig mongoConfig = new MongoInteraction.MongoConfig(System.getenv("MONGO_HOST"),
                Integer.parseInt(System.getenv("MONGO_PORT")),
                System.getenv("MONGO_DB"),
                System.getenv("MONGO_USER"),
                System.getenv("MONGO_PWD"));

        MongoInteraction interaction = new MongoInteraction(mongoConfig);

        Rabbit.RabbitConfig rabbitConfig = new Rabbit.RabbitConfig(System.getenv("RABBIT_HOST"),
                Integer.parseInt(System.getenv("RABBIT_PORT")),
                System.getenv("RABBIT_USER"),
                System.getenv("RABBIT_PWD"),
                System.getenv("QUEUE_NAME"));
        Rabbit rabbit = new Rabbit(rabbitConfig);

        DBCursor cursor = interaction.getAllValues(System.getenv("SERVICE_NAME"));

        while (cursor.hasNext()) {
            DBObject obj_from = cursor.next();
            JSONObject obj = new JSONObject(JSON.serialize(obj_from));
            if (obj_from.get("to") != null && obj_from.get("from")!= null) {
                List<Ticket> result = requestToFindTickets(obj);
                if (result.size() != 0) {
                    JSONObject message = new JSONObject();
                    message.put("chat_id", obj.getString("chat_id"));
                    message.put("username", obj.getString("username"));
                    ArrayList<JSONObject> tickets = new ArrayList<>();
                    for (Ticket s: result) {
                        JSONObject ticket = new JSONObject();
                        ticket.put("from", s.from);
                        ticket.put("to", s.to);
                        ticket.put("ticket" , s.ticket);
                        tickets.add(ticket);
                    }
                    message.put("tickets", tickets);
                    rabbit.sendMessage(message.toString());
                }
            }

        }
    }

    public static String checkTickets(String from, String to) throws Exception{

        String From = sendGet(String.format("https://www.travelpayouts.com/widgets_suggest_params?q=%s",from));
        String To = sendGet(String.format("https://www.travelpayouts.com/widgets_suggest_params?q=%s",to));
        JSONObject from_result = new JSONObject(From);
        JSONObject to_result = new JSONObject(To);

        JSONObject from_capital = (JSONObject) from_result.get("capital");
        JSONObject to_capital = (JSONObject) to_result.get("capital");

        //String origin = sendGet(String.format(""));
        String origin = from_capital.getString("iata");
        String destination = to_capital.getString("iata");

        String req = String.format("http://api.travelpayouts.com/v1/prices/direct?currency=rub&origin=%s&destination=%s&period_type=%s&page=1&limit=1&show_to_affiliates=true&sorting=price&token=%s",origin, destination , "year", "ed251b41f664daa0911f856af1f0f2f0" );

        return sendGet(req);
    }

    private static String sendGet(String req) throws Exception {
        HttpGet request = new HttpGet(req);
        CloseableHttpClient httpCli = HttpClients.createDefault();
        try (CloseableHttpResponse response = httpCli.execute(request);) {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String result = EntityUtils.toString(entity);
                return result;
            }
        }
        return null;
    }

}
